from functools import reduce


# Generate word list from file words.txt
word_list = []

with open("words.txt", "r") as f:
    for word in f:
        word_list.append(word[:-1])


def gematria(word):
    """
    Each word is reduced to a sum of the values of the letters assuming A = 1,
    B = 2 and so on.
    """
    return reduce(
        lambda total, current: total + ord(current) - 64,
        [c for c in word.upper()],
        0,
    )


# A test case checking which 6-letter words reduce to 66
for w in word_list:
    x = gematria(w)
    if x == 66 and len(w) == 6:
        print(w)
