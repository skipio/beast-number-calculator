## GEMATRIA CALCULATOR

I developed a short bit of code to calculate the values of english words based on a modified application of the principle of [Gematria](https://en.wikipedia.org/wiki/Gematria) on the English alphabet with A = 1, B = 2 and so on.

The word list was pulled from [dwyl's repo](https://github.com/dwyl/english-words).
